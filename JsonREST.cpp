#include <JsonREST.h>
#include <ESP8266WiFi.h>


Link::Link(const std::string& href, Link* parent, 
    const std::vector<std::string>& href_docs)
    :href(href), children({}), parent(parent), docs(href_docs){
    if(!docs.size())
        docs.push_back(href);
}
void Link::assignChildren(const std::vector<Link*>& links){
    for(Link* link : links){
        if(link->parent == nullptr)
            continue;
        link->parent->children.push_back(link);        
    }
}
void _prependParentPart(Link* link, std::vector<Link*>& visited){
    bool has_visited = false;
    for(Link* vis : visited)
        has_visited = has_visited || (vis == link);
    if(has_visited)
        return;
    visited.push_back(link);
    if(link->parent == nullptr)
        return;
    _prependParentPart(link->parent, visited);
    link->href = link->parent->href + "/" + link->href;

}
void Link::prependParentPart(const std::vector<Link*>& links){
    std::vector<Link*> visited;
    for(Link* link : links){
        _prependParentPart(link, visited);
    }
}


Rule::Rule(Link* link, response_hook _get, response_hook _post, 
        response_hook _put, response_hook _delete)
    :link(link), GET(_get), POST(_post), PUT(_put), DELETE(_delete){
}

bool Rule::matchesURL(const std::string& url, std::vector<std::string>& args) const{
    if(link == nullptr)
        return false;
    args.clear();

    std::vector<std::string> url_tokens;
    std::vector<std::string> rule_tokens;
    JsonREST::splitString(url, "/", url_tokens);
    JsonREST::splitString(link->href, "/", rule_tokens);
    
    if(url_tokens.size() != rule_tokens.size())
        return false;

    for(short tok_i=0; tok_i<rule_tokens.size(); ++tok_i){
        if(rule_tokens[tok_i] == "%s"){
            args.push_back(url_tokens[tok_i]);
        }else if(rule_tokens[tok_i] != url_tokens[tok_i]){
            return false;
        }
    }
    return true;
}

Request::Request(const std::string& method, const std::string& url, const std::string& http)
    :method(method), url(url), http(http){
}

Response::Response()
    :successful(true), code(200), code_name("OK"), error_msg(""), request(),
        data(""), header(""), rule(), links({}), args({}), param_keys({}),      
        param_vals({}), post_keys({}), post_vals({}){
}
void Response::error(short _code, const std::string& name, const std::string& _msg){
    successful = false;
    code = _code;
    code_name = name;
    error_msg = _msg;
}
void Response::success(short _code, const std::string& name, const std::string& _msg){
    successful = true;
    code = _code;
    code_name = name;
    error_msg = _msg;
}


JsonREST::JsonREST()
    :rules({}), error(""), invalid_cb(NULL), cur_resp(NULL){}

JsonREST::~JsonREST(){
    if(cur_resp != NULL)
        delete cur_resp;
}

void JsonREST::addRule(Rule rule){
    rules.push_back(rule);
}
void JsonREST::addRules(const std::vector<Rule>& _rules){
    rules.insert(rules.end(), _rules.begin(), _rules.end());
}
void JsonREST::createLinkHierarchy(){
    std::vector<Link*> links;
    for(auto& rule: rules)
        if(rule.link != nullptr){
            links.push_back(rule.link);
        }
    Link::assignChildren(links);
}
void JsonREST::completeLinkHref(){
    std::vector<Link*> links;
    for(auto& rule: rules)
        if(rule.link != nullptr){
            links.push_back(rule.link);
        }
    Link::prependParentPart(links);
}

void JsonREST::setInvalidCallback(response_hook resp_hk){
    invalid_cb = resp_hk;
}

void JsonREST::handleClient(WiFiClient& client){
    std::string req = client.readStringUntil('\r').c_str();
    Response* resp = new Response();
    resp->request.url = req;

    std::vector<std::string> req_tokens;
    JsonREST::splitString(req, " ", req_tokens);
    if(req_tokens.size() != 3){
        resp->error(400, "Bad Request", "Unexpected request format. Expected '"
                    "<method>, <URI>, <Protocol>'");
        handleRequest("", "", resp);
        return;
    }
    resp->request.method = req_tokens[0];
    REST_UTILITY::toUpperCase(resp->request.method);
    resp->request.url = req_tokens[1];
    resp->request.http = req_tokens[2];

    if(resp->request.method != "GET"){
        while(client.available()){
            std::string line = client.readStringUntil('\n').c_str();
            if(line == "\r"){
                std::string body = client.readString().c_str();
                JsonREST::getQueryParts(body, resp->post_keys, resp->post_vals);
                break;
            }
        }
    }

    if(resp->request.method == "POST"){
        short method_index = REST_UTILITY::findIndexOf(resp->post_keys, "method");
        if(method_index != -1){
            std::string override_method = resp->post_vals[method_index];
            REST_UTILITY::toUpperCase(override_method);
            if(override_method == "PUT" || override_method == "DELETE"){
                resp->request.method = override_method;
            }
        }
    }

    handleRequest(resp->request.method, resp->request.url, resp);
}

void JsonREST::handleRequest(const std::string& method, const std::string& url, Response* resp){
    if(cur_resp != NULL)
        delete cur_resp;
    if(resp == NULL){
        cur_resp = new Response();
        cur_resp->request.method = method;
        cur_resp->request.url = url;
        cur_resp->request.http = "Direct Method Call";
    }
    else
        cur_resp = resp;

    if(method == ""){
        invalid_cb();
        return;        
    }

    std::string url_copy = url;
    char* reqchar = &url_copy[0u];
    struct yuarel url_parts;
    if(yuarel_parse(&url_parts, reqchar) == -1){
        cur_resp->error(400, "Bad Request", "Invalid URI format: '" + url + "'");
        return;
    }

    std::vector<std::string> args;
    response_hook resp_func = NULL;

    if(url_parts.query != NULL)
        JsonREST::getQueryParts(url_parts.query, cur_resp->param_keys, 
                                cur_resp->param_vals);

    if(url_parts.path != NULL){
        for(auto& rule : rules){
            if(rule.matchesURL(url_parts.path, args)){
                if(method == "GET" && rule.GET != NULL)
                    resp_func = rule.GET;
                else if(method == "POST" && rule.POST != NULL)
                    resp_func = rule.POST;
                else if(method == "DELETE" && rule.DELETE != NULL)
                    resp_func = rule.DELETE;
                else if(method == "PUT" && rule.PUT != NULL)
                    resp_func = rule.PUT;
                cur_resp->rule = rule;
                break;
            }
        }
    }

    cur_resp->args = args;
    if(resp_func == NULL){
        cur_resp->error(404, "Not Found", "No match found for URI '" + url + "'");

        if(invalid_cb != NULL)
            invalid_cb();
        return;
    }
    resp_func();
}


Response* JsonREST::getResponse(){
    return cur_resp;
}


std::string JsonREST::getResponseJsonStr(bool pretty){
    DynamicJsonBuffer json_buf;
    JsonObject& json_obj = json_buf.createObject();
    Response* resp = getResponse();

    json_obj["success"] = resp->successful;
    std::string status = REST_UTILITY::intToStr(resp->code) + " " + resp->code_name;
    json_obj["status"] = status;
    json_obj["data"] = RawJson(resp->data);

    JsonObject& req = json_obj.createNestedObject("request");
    req["method"] = resp->request.method;
    req["url"] = resp->request.url;
    req["http"] = resp->request.http;

    if(!resp->successful)
        json_obj["error"] = resp->error_msg;

    JsonArray& links = json_obj.createNestedArray("links");
    for(const auto& link: resp->links){
        for(const auto& doc : link.docs)
            links.add(doc);
    }

    std::string jsonstr;
    if(pretty){
        json_obj.prettyPrintTo(jsonstr);
    }else{
        json_obj.printTo(jsonstr);
    }
    return jsonstr;
}


std::string& JsonREST::getError(){
    std::string err(error);
    error = "";
    return err;
}


void JsonREST::splitString(const std::string& str, const std::string& delim, 
                            std::vector<std::string>& parts, short max_split){
    char* pch;
    short index = 0;
    const char* cdelim = delim.c_str();
    std::string _str = str;
    char* cstr = &_str[0u];
    pch = strtok(cstr, cdelim);

    while(pch != NULL){
        parts.push_back(std::string(pch));
        pch = strtok(NULL, cdelim);
        max_split--;
        if(max_split == 0)
            break;
    }
}

void JsonREST::getQueryParts(const std::string& query, std::vector<std::string>& keys, 
                            std::vector<std::string>& vals){
    std::vector<std::string> keyvals;
    std::vector<std::string> keyval;
    JsonREST::splitString(query, "&", keyvals);

    for(const auto& keyandval : keyvals){
        JsonREST::splitString(keyandval, "=", keyval);
        if(keyval.size() == 2){
            keys.push_back(keyval[0]);
            vals.push_back(keyval[1]);
        }
        keyval.clear();
    }
}

namespace REST_UTILITY{
    short findIndexOf(const std::vector<std::string>& vec, const std::string& str) {
        for (short i = 0; i < vec.size(); ++i) {
        if (vec[i] == str)
          return i;
        }
        return -1;
    }

    void toUpperCase(std::string& str){
        for(auto& c: str) 
            c = toupper(c);
    }

    std::string intToStr(int x){
        int size = snprintf(nullptr, 0, "%i", x);
        std::string str_num;
        str_num.resize(size);
        snprintf(&str_num[0], size+1, "%i", x);
        return str_num;
    }
}
