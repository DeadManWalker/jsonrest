#ifndef JSONREST_H
#define JSONREST_H

#define ARDUINOJSON_EMBEDDED_MODE 0
#include <ArduinoJson.h>

#include <vector>
#include <string>
#include <yuarel.h>


class WiFiClient;
struct Response;

typedef void(*response_hook)(void);
typedef void(*invalid_hook)(void);


struct Link{
    Link(const std::string& href="", Link* parent=nullptr, 
        const std::vector<std::string>& href_docs = std::vector<std::string>());

    Link* parent;
    std::vector<Link*> children;
    std::string href;
    std::vector<std::string> docs;

    static void assignChildren(const std::vector<Link*>& links);
    static void prependParentPart(const std::vector<Link*>& links);
};
struct Rule{
    Rule(Link* link=NULL, response_hook _get=NULL, response_hook _post=NULL, 
        response_hook _put=NULL, response_hook _delete=NULL);
    Link* link;
    response_hook GET;
    response_hook POST;
    response_hook PUT;
    response_hook DELETE;

    bool matchesURL(const std::string& url, std::vector<std::string>& args) const;
};
struct Request{
    Request(const std::string& method="", const std::string& url="", const std::string& http="");

    std::string method;
    std::string url;
    std::string http;
};
struct Response{
    Response();

    void error(short code, const std::string& name, const std::string& msg);
    void success(short code, const std::string& name, const std::string& msg);

    bool successful;
    short code;
    std::string code_name;
    std::string data;
    std::string error_msg;
    std::string header;
    Request request;
    Rule rule;
    std::vector<Link> links;
    std::vector<std::string> args;
    std::vector<std::string> param_keys;
    std::vector<std::string> param_vals;
    std::vector<std::string> post_keys;
    std::vector<std::string> post_vals;
};



class JsonREST{

public:
    JsonREST();
    ~JsonREST();

    void handleClient(WiFiClient& client);
    void handleRequest(const std::string& method, const std::string& url, Response* resp=NULL);

    Response* getResponse();
    std::string getResponseJsonStr(bool pretty=false);

    void addRule(Rule rule);
    void addRules(const std::vector<Rule>& rules);
    void createLinkHierarchy();
    void completeLinkHref();
    void setInvalidCallback(response_hook);
    
    std::string& getError();
    void updateJson();
    
    static void splitString(const std::string& str, const std::string& delim, 
                            std::vector<std::string>& parts, short max_split=0);
    static void getQueryParts(const std::string& query, std::vector<std::string>& keys, 
                            std::vector<std::string>& vals);
   
    
protected:
    std::vector<Rule> rules;
    std::string error;
    invalid_hook invalid_cb;
    Response* cur_resp;
};

namespace REST_UTILITY{
    short findIndexOf(const std::vector<std::string>& vec, const std::string& str);
    void toUpperCase(std::string& str);
    std::string intToStr(int x);
}

#endif
